#!/bin/sh
set -e

variant=$1
image=docker.io/msrd0/rust:$variant

APK_INSTALL=
APT_INSTALL=
CARGO_INSTALL=
RUSTUP_COMPONENTS=
RUSTUP_TOOLCHAIN=

while [ $# -gt 0 ]
do
	shift
	case "$1" in
		
		stable|beta|nightly)
		RUSTUP_TOOLCHAIN=$1
		image=$image-$1
		;;
		
		clippy)
		if [ "$variant" = "rustup" ]
		then
			RUSTUP_COMPONENTS="$RUSTUP_COMPONENTS clippy"
		else
			CARGO_INSTALL="$CARGO_INSTALL cargo-clippy"
		fi
		image=$image-clippy
		;;
		
		deny)
		CARGO_INSTALL="$CARGO_INSTALL cargo-deny"
		image=$image-deny
		;;
		
		diesel)
		if [ "$variant" = "rustup" ]
		then
			APT_INSTALL="$APT_INSTALL patch libpq-dev libsqlite3-dev libmariadb-dev-compat"
		else
			APK_INSTALL="$APK_INSTALL patch postgresql-dev sqlite-dev mariadb-connector-c-dev"
		fi
		CARGO_INSTALL="$CARGO_INSTALL diesel_cli"
		image=$image-diesel
		;;
		
		readme)
		CARGO_INSTALL="$CARGO_INSTALL cargo-readme"
		image=$image-readme
		;;
		
		tarpaulin)
		if [ "$variant" = "rustup" ]
		then
			APT_INSTALL="$APT_INSTALL libssl-dev"
		else
			APK_INSTALL="$APK_INSTALL libressl-dev"
		fi
		CARGO_INSTALL="$CARGO_INSTALL cargo-tarpaulin"
		image=$image-tarpaulin
		;;
		
		sweep)
		CARGO_INSTALL="$CARGO_INSTALL cargo-sweep"
		image=$image-sweep
		;;
		
	esac
done

build_args=
if [ -n "$APK_INSTALL" ]; then build_args="$build_args --build-arg APK_INSTALL='${APK_INSTALL## }'"; fi
if [ -n "$APT_INSTALL" ]; then build_args="$build_args --build-arg APT_INSTALL='${APT_INSTALL## }'"; fi
if [ -n "$CARGO_INSTALL" ]; then build_args="$build_args --build-arg CARGO_INSTALL='${CARGO_INSTALL## }'"; fi
if [ -n "$RUSTUP_COMPONENTS" ]; then build_args="$build_args --build-arg RUSTUP_COMPONENTS='${RUSTUP_COMPONENTS## }'"; fi
if [ -n "$RUSTUP_TOOLCHAIN" ]; then build_args="$build_args --build-arg RUSTUP_TOOLCHAIN='${RUSTUP_TOOLCHAIN## }'"; fi
docker="docker build $build_args -t $image $variant"
echo $docker
eval $docker 1>&2
echo $image
